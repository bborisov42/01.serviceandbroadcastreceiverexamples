package com.innovator.serviceandbroadcastreceiverexamples;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class NumberMeaningIntentService extends IntentService {

    public static final String ACTION_NUMBER_GET_MEANING = "com.innovator.serviceandbroadcastreceiverexamples.action.NUMBER_GET_MEANING";
    public static final String EXTRA_NUMBER_MEANING = "com.innovator.serviceandbroadcastreceiverexamples.extra.NUMBER_MEANING";

    private static final String ACTION_NUMBER_FIND_MEANING = "com.innovator.serviceandbroadcastreceiverexamples.action.NUMBER_FIND_MEANING";
    private static final String EXTRA_NUMBER = "com.innovator.serviceandbroadcastreceiverexamples.extra.NUMBER";

    private static final String BASE_ADDRESS = "http://numbersapi.com";

    public NumberMeaningIntentService() {
        super("NumberMeaningIntentService");
    }

    public static void startActionGetNumberMeaning(Context context, int number) {
        Intent intent = new Intent(context, NumberMeaningIntentService.class);
        intent.setAction(ACTION_NUMBER_FIND_MEANING);
        intent.putExtra(EXTRA_NUMBER, number);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_NUMBER_FIND_MEANING.equals(action)) {
                final int number = intent.getIntExtra(EXTRA_NUMBER, 42);
                handleActionGetNumberMeaning(number);
            }
        }
    }

    private void handleActionGetNumberMeaning(int number) {
        if (NetworkUtils.isNetworkAvailable(this)) {
            List<String> pathSegments = new ArrayList<>();
            pathSegments.add("" + number);
            URL url = NetworkUtils.buildUrl(BASE_ADDRESS, pathSegments, null);
            try {
                String result = NetworkUtils.getResponseFromHttpGetUrl(url);

                Intent intent = new Intent();
                intent.setAction(ACTION_NUMBER_GET_MEANING);
                intent.putExtra(EXTRA_NUMBER_MEANING, result);

                sendBroadcast(intent);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
