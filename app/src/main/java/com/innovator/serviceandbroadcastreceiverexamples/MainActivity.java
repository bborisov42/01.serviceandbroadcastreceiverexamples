package com.innovator.serviceandbroadcastreceiverexamples;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText inputEditText;
    private TextView resultTextView;

    private IntentFilter numberMeaningIntentFilter = new IntentFilter(NumberMeaningIntentService.ACTION_NUMBER_GET_MEANING);
    private BroadcastReceiver numberMeaningBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            resultTextView.setText(intent.getStringExtra(NumberMeaningIntentService.EXTRA_NUMBER_MEANING));
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inputEditText = findViewById(R.id.input_et);
        resultTextView = findViewById(R.id.result_tv);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(numberMeaningBroadcastReceiver, numberMeaningIntentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(numberMeaningBroadcastReceiver);
    }


    public void findMeaning(View view) {
        CharSequence result = inputEditText.getText();

        if (TextUtils.isDigitsOnly(result)) {
            NumberMeaningIntentService.startActionGetNumberMeaning(this, Integer.parseInt(result.toString()));
        }
    }
}
